Integrate azimuthal slices from Pilatus-like data

# Contents
- [TODO](#todo)
- [Getting started](#start)
  - [option 1: interactive session](#s1)
  - [option 2: batch file](#s2)
- [Some help](#ex1)


# TO-DO list <a name="todo"></a>

- add option to use configuration file besides command-line arguments, as done in pdfgetx3 and others

- solve problem with changing ChiDisc within the same scan and between scans when working on GPU. This is related to a MemoryError during sparse integrator initialization when trying to allocate buffers

- write a 2d method for GPU in order to speed up medfilt1d

- add functionality to pass an array of masks, in order to apply a different mask to each data frame. This might be very slow on GPU if it forces reinitialization of the sparse integrator.

- change behaviour of --maxframes to specify a range instead (e.g. images 2 to 200). Avoid overlap with --expect

- next release should be v1.0 or something like that. Or switch to "year" version number


# Getting started <a name="start"></a>
To use the GPU processing speed (~300 frames per second), the program needs to run on a dedicated GPU machine, which can be accessed through the ESRF **slurm** system. For more info: https://confluence.esrf.fr/display/SCKB/SLURM+Commands

## option 1: interactive session <a name="s1"></a>
Start a session on a GPU machine within the same terminal (i.e. interactive session) using the command `salloc`. For example:
```
salloc -p gpu --mem=32G --time=2:00:00 --x11 --gres=gpu:1 srun --pty bash -i
```
means asking for a node on the partition called `gpu`, for 2 hours, with 1 GPU card, in interactive mode.

The interactive session is closed 
1. at the end of the `--time` window (12 hours on partition `gpu` or 48 hours on `low-gpu`, which is a lower-priority version of `gpu`) 
2. if the terminal is closed (e.g. deliberate closing, network glitch).

To be able to use hdintrange with GPU, activate the dedicated python environment:
```
source /data/id15/inhouse6/hdintrange/latest/bin/activate
```
'latest' is a symlink to the most recently developed version. As of June 2024, this is v13. 

In command line, 'hdintrange' is an alias of the respective 'hdintrange_v??' executable.

## option 2: batch file <a name="s2"></a>
The `hdintrange` command can also be embedded in a batch file, which is a script requesting cluster resources first, and then executing the code therein. For example, set up a file with a single integration command like this:

`cat test_slurm.sbatch`
```
#!/bin/bash
#
#SBATCH --partition=gpu
#SBATCH --ntasks=1
#SBATCH --gpus=1
#SBATCH --mem=32G
#
source /data/id15/inhouse6/hdintrange/latest/bin/activate
#
hdintrange SNN_Z0/SNN_Z0_von_pp1600V/scan0003 -p calibration_CeO2_Position3/CeO2_85000eV_2100mm_s1.poni -m basemask22+2px.edf -o aztest --force --nogpu
```
And then execute with the requested resources: <br>
```
sbatch test_slurm.sbatch
```

Resource allocation is terminated at the end of the process, regardless of any `--time` argument used in the batch file.


# Some help <a name="ex1"></a>
Show help
```
hdintrange -h
```
Requirements to run hdintrange:
- activate environment (see above)
- logged in on a GPU machine (see above)
- change directory to the experiment base directory, e.g. /data/visitor/ch0405/id15a/20240119
- provide scans as arguments in this format: sample/sample_dataset/scan. E.g. SNN_Z0/SNN_Z0_von_pp1600V/scan0003
- provide poni file after "-p", e.g. -p calibration_CeO2_Position3/CeO2_85000eV_2100mm_s1.poni. This can also be an absolute path.
- provide mask file after "-m", e.g. -m basemask22+2px.edf. This can also be an absolute path.

All other arguments are optional. Please read help and check last the last used commands in your experiment folder and processed HDF5 files ("azint").



