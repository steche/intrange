import numpy as np
import matplotlib.pyplot as plt
plt.ioff()

##########################################
#### Data for CdTe from XCOM database
#### ene_keV is photon energy in keV
#### sig_cm2_g is total attenuation including coherent scattering in cm2/g
##########################################
ene_keV = np.array([  5.  ,   6.  ,   8.  ,  10.  ,  15.  ,  20.  ,  26.71,  26.71,
                     30.  ,  31.81,  31.81,  40.  ,  50.  ,  60.  ,  70.  ,  80.  ,
                     90.  , 100.  , 110.  , 120.  , 130.  , 140.  ])

sig_cm2_g = np.array([8.392e+02, 5.286e+02, 2.492e+02, 1.381e+02, 4.656e+01, 2.144e+01,
                      9.832e+00, 2.943e+01, 2.182e+01, 1.874e+01, 3.494e+01, 1.930e+01,
                      1.067e+01, 6.542e+00, 4.321e+00, 3.019e+00, 2.206e+00, 1.671e+00,
                      1.305e+00, 1.046e+00, 8.566e-01, 7.152e-01])

######################################################################
#### density of CdTe in g/cm3
#### thickness in cm of CdTe sensor in Pilatus3 CdTe
sensor_density = 5.85
sensor_thickness = 0.1

######################################################################
ene_grid = np.arange(ene_keV[0], ene_keV[-1]+1e-3, 0.1)
spl = np.interp(ene_grid, ene_keV, np.log10(sig_cm2_g))
spl_lin = 10**spl
mu_lin = spl_lin * sensor_density
ene_eV = np.around(ene_grid*1e3, 0)
header = '## data from XCOM database. mass attenuation coefficients (cm2/g) were multiplied by a density of 5.85 g/cm3'
np.savetxt('cdte_mu_cm-1.dat', np.transpose([ene_eV, mu_lin]), fmt=['%i','%.5e'],
header=header)

plot = 1
if plot == 1:
    plt.figure()
    plt.title('data from XCOM')
    plt.semilogy(ene_keV, sig_cm2_g, 'co-')
    plt.ylabel('semilogy of raw values')
    plt.twinx()
    plt.plot(ene_keV, np.log10(sig_cm2_g), 'k.')
    plt.ylabel('log10(values) on linear axis')
    plt.xlabel('E / keV')

    plt.figure()
    plt.title('after fit')
    plt.plot(ene_grid, spl, 'r.', label='fit')
    plt.plot(ene_keV, np.log10(sig_cm2_g), 'kx', label='data')
    plt.figure()
    plt.title('linearized')
    plt.semilogy(ene_grid, spl_lin, 'r.', label='fit')
    plt.semilogy(ene_keV, sig_cm2_g, 'co-', label='data')
    plt.show()
