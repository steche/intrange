#!/usr/bin/python3

import sys, os, time
import logging
import numpy as np
import h5py, pyFAI
from pyFAI.method_registry import IntegrationMethod
from tqdm import tqdm
from hdintrange.parse_arguments import parse_arguments
from hdintrange.browse import whereami, make_filelist_HDF5
from hdintrange.azsetup import make_ai, make_detector_atten, make_azims
from hdintrange.base import GPU_prm, get_positions, CustomFormatter, \
                     memsize, show_memsize, new_engine, xconv
from timeit import timeit
import signal
import warnings
import traceback

warnings.filterwarnings("ignore", category=RuntimeWarning)

try:
    import pyopencl.array as cla
    from silx.opencl.codec.bitshuffle_lz4 import BitshuffleLz4
except:
    LGR.critical('before retrying, make sure to: \
1) activate the env with silx>=2 and pyfai>=2023 \
2) are on an interactive gpu session (salloc -p gpu --time=8:00:00 --x11 --gres=gpu:1 srun --pty bash -i )')
    sys.exit(0)

# reduce output by imported modules
logging.getLogger('pyFAI').setLevel(30)
logging.getLogger('h5py').setLevel(30)
logging.getLogger('fabio').setLevel(30)


class Integrator:


    def __init__(self, args):
        self.t = [time.perf_counter()]
        self.version = os.path.basename(__file__)
        self.args = args
        self.scans, self.dims, self.somefile = {}, [0], None
        self.arraypath = '/entry_0000/measurement/data'
        self.camerapath = f'measurement/{args.camera}'
        self.flags = {'processor':None, 'case':None, 'function':None}
        self.ai, self.aimethod = None, None
        self.det_att, self.azims = None, None
        self.si_xrange = None
        self.pola = 0.99
        self.gpu = GPU_prm()
        self.corrections = {}
        self.checksums = {}
        signal.signal(signal.SIGQUIT, self._handle_sigquit)
        for ii in self.__dict__:
            LGR.debug(f'{ii} : {getattr(self, ii)}')
        self._timeit('Reading arguments')


    def _timeit(self, message):
        tt = time.perf_counter()
        LGR.critical(f'{message} -- {np.around(1e3*(tt-self.t[-1]),1):.0f} ms')
        self.t.append(tt)


    def _handle_sigquit(self, signum, frame):
        sys.stdout.flush()
        LGR.warning('Emergency exit (ctrl+\\)')
        sys.stdout.flush()
        sys.exit(0)


    def set_azims(self):
        self.azims = make_azims(self.args.azim[0], self.args.azim[1], self.args.azim[2])
        self._timeit('Setting azimuthal ranges')


    def set_filelist(self):
        fate = ['skipped','accepted']
        self.scans = make_filelist_HDF5(self.args.datadirs, self.args.camera)
        nfiles = 0
        nimg = None
        for sc in self.scans:
            self.scans[sc].outname = '/'.join([self.args.outdir,
                                               self.scans[sc].outsubdir,
                                               self.scans[sc].outprefix])
            if self.args.expect <= 0:
                self.scans[sc].accept = 0
            elif os.path.isfile(self.scans[sc].outname) is True and self.args.force is False:
                self.scans[sc].accept = 0
            else:
                try:
                    with h5py.File(self.scans[sc].upperh5, 'r') as fil:
                        target = fil[f'{self.scans[sc].scan_number}.1/{self.camerapath}']
                        nimg = target.shape[0]
                        self.scans[sc].tot_size_gb = target.nbytes / (1<<30)
                        self.scans[sc].tot_n_img = nimg
                        nfiles += len(self.scans[sc].fnames)
                        self.dims[0] += nimg
                except KeyError as err:
                    LGR.debug(f'NO DATA {sc}')
                    self.scans[sc].tot_n_img = 0
                    self.scans[sc].accept = 0
                except OSError as err:
                    LGR.debug(f'{sc} {err}')
                    self.scans[sc].tot_n_img = 0
                    self.scans[sc].accept = 0
                if self.scans[sc].tot_n_img >= self.args.expect:   # tot_n_img at least 1
                    self.scans[sc].accept = 1
                    # self.somefile = self.scans[sc].fnames[0]
                    self.somefile = np.random.choice(self.scans[sc].fnames)  #somefile is used for gpu initialisation. its size does not matter
                else:  # e.g. broken scans
                    self.scans[sc].accept = 0
#            ## if unset, set maxframes to number of images found  # 2024-dec-12: bad choice; when integrating scans with different lengths it forces the value of the first one onto the others
#            if isinstance(nimg, int) and not self.args.maxframes:
#                self.args.maxframes = self.scans[sc].tot_n_img
#                frames_per_file = nimg//nfiles
#                newval = frames_per_file * max([1, round(self.args.maxframes/frames_per_file)])
#                LGR.debug(f'Maxframes reset from {self.args.maxframes} to {newval} to match chunksize')
#                self.args.maxframes = newval
            LGR.critical(f"""---- {self.scans[sc].scankey}: {fate[self.scans[sc].accept]}  ({self.scans[sc].tot_n_img:d}/{self.args.expect:d}) ----""")
        self._timeit(f'Finding {nfiles:d} datafiles / {self.dims[0]:d} images')


    def set_ai(self):
        """
        Set up pyFAI azimuthal integrator using default or custom:
            1) poni file (.poni)
            2) mask file (.edf)
            3) nexus detector definition (.h5)
            4) flat-field file (.edf)
        When the radial range limits are not set in the input arguments, they \
        are extracted as the minimum and the maximum of the Q or 2theta array \
        (depending on the choice of radial units) known to the integrator. \
        These are both in the final units (Q: A^-1, 2theta: deg) and in standard \
        units (Q: nm^-1, 2theta:rad) so that calls to function 'setup_sparse_integrator' \
        will impose this radial range (pos0_range), work in standard units (by setting scale=False) \
        and finally converting to the selected units.
        """
        self.ai = make_ai(self.args.ponifile, self.args.maskfile,
                          self.args.nexusdetfile, self.args.flat)
        if self.args.unit == 'q_A^-1':
            sivals = np.ravel(self.ai.qArray())
            radialvals = 0.1*sivals
            lab = 'q_nm^-1'
        elif self.args.unit == '2th_deg':
            sivals = np.ravel(self.ai.twoThetaArray())
            radialvals = np.degrees(sivals)
            lab = '2th_rad'
        if self.args.xrange is None:
            self.args.xrange = (min(radialvals), max(radialvals))
            self.si_xrange = (min(sivals), max(sivals))
            LGR.debug(f'Radial range reset from None to {self.args.xrange} {self.args.unit}')
        else:
            self.si_xrange = (sivals[np.argmin(abs(self.args.xrange[0]-radialvals))],
                              sivals[np.argmin(abs(self.args.xrange[1]-radialvals))])
        LGR.debug(f'Radial range in SI units: {self.si_xrange} {lab}')
        self._timeit('Setting pyfai integrator')


    def set_method(self):
        """
        Selects a method based on what's possible with the available hardware \
        and the input arguments. Methods are tried in order of preference, \
        roughly corresponding to speed on an average ESRF machine. \
        This should give a decent choice both in the case of GPU and CPU.
        The --nogpu option has no effect on this, since method is independent of gpu decompression.
        With --cython option active, cython (non-gpu) methods are tried first.
        """
        allmeths = [mm for mm in IntegrationMethod.select_method(dim=1, split=self.args.split)]
        meths = []
        if self.args.cython is not None:
            pass
        else: # gpu/opencl methods
            meths += [mm for mm in allmeths if mm.target_type == 'gpu' and mm.algo_lower == 'csr']
            meths += [mm for mm in allmeths if mm.target_type == 'gpu' and mm.algo_lower == 'lut']
            meths += [mm for mm in allmeths if mm.target_type == 'gpu' and mm.algo_lower == 'histogram']
        # add non-gpu methods
        meths += [mm for mm in allmeths if mm.implementation == 'cython' and mm.algo_lower == 'csc']
        meths += [mm for mm in allmeths if mm.implementation == 'cython' and mm.algo_lower == 'csr']
        meths += [mm for mm in allmeths if mm.implementation == 'cython' and mm.algo_lower == 'lut']
        meths += [mm for mm in allmeths if mm.implementation == 'cython' and mm.algo_lower == 'histogram']
        meths += [mm for mm in allmeths if mm.target_type == 'cpu' and mm.algo_lower == 'csr']
        meths += [mm for mm in allmeths if mm.target_type == 'cpu' and mm.algo_lower == 'lut']
        meths += [mm for mm in allmeths if mm.target_type == 'cpu' and mm.algo_lower == 'histogram']
        meths += [mm for mm in allmeths if mm.implementation == 'python' and mm.algo_lower == 'csr']
        meths += [mm for mm in allmeths if mm.implementation == 'python' and mm.algo_lower == 'histogram']
        if self.args.medianfilter is None and self.args.sigmafilter is None:
            self.flags['case'] = 'integrate'
            self.flags['function'] = self.ai.integrate1d
            caseargs = {'error_model':self.args.error}
        elif self.args.medianfilter is not None and self.args.sigmafilter is None:
            self.flags['case'] = 'medfilt'
            self.flags['function'] = self.ai.medfilt1d
            caseargs = {'npt_azim':self.args.medianfilter[-1]}
        elif self.args.sigmafilter is not None:
            self.flags['case'] = 'sigma_clip'
            self.flags['function'] = self.ai.sigma_clip
            caseargs = {'error_model':self.args.error}
        ## test method with flat image. In the future a 'perfect' pattern could be used, e.g. generated by pyfai fake_calibration_image()
        # dummy_im = np.ones(self.ai.detector.shape)*1e6
        ## test method with Q image. Integration result should give radial=intensity
        dummy_im = self.ai.qArray()/10
        toler = 1e-0
        for ind, mm in enumerate(meths):
            faillog = ''
            resid = np.inf
            try:
                tres = self.flags['function'](dummy_im, self.args.npt, method=mm,
                                              unit='q_A^-1', correctSolidAngle=False,
                                              polarization_factor=None, **caseargs)
                resid = np.sqrt(np.sum(np.square(tres.radial-tres.intensity)))/self.args.npt
                if resid > toler : raise Exception
                LGR.debug(f'Method {mm} succeeded with {self.flags["function"].__name__} (mean error/bin={resid:.3e}, should be < {toler:.3e})')
                self.aimethod = mm
                break
            except Exception as err:
                failmsg = f'Method {mm} failed with {self.flags["function"].__name__}  (mean error/bin={resid:.3e}, should be < {toler:.3e})'
                LGR.debug(failmsg)
                LGR.debug(err)
                faillog += f'{failmsg}--{err}\n'
        if not self.aimethod:
            LGR.error('None of the available integration methods worked. Exiting')
            LGR.error(faillog)
            sys.exit(0)
        self.flags['processor'] = self.aimethod.target_type
        LGR.debug(f'Flags: {self.flags}')
        LGR.critical(f'Method: {self.aimethod}')


    def set_corrections(self):
        ene_ev = pyFAI.units.hc/self.ai.wavelength/1e7
        tth_rad = self.ai.integrate1d_ng(np.ones(self.ai.detector.shape),
                                         self.args.npt, unit='2th_rad',
                                         polarization_factor=self.pola,
                                         error_model=self.args.error,
                                         method=self.aimethod)[0]
        LGR.debug(str(tth_rad))
        LGR.debug(np.round(ene_ev,2))
        self.det_att = make_detector_atten(ene_ev, tth_rad)
        ###--- make arrays and respective checksums
        self.corrections['polarization'] = self.ai.polarization(shape=self.ai.detector.shape, factor=self.pola)
        if self.args.solidangle is True:
            self.corrections['solidangle'] = self.ai.solidAngleArray(self.ai.detector.shape)
        else:
            self.corrections['solidangle'] = self.ai.solidAngleArray(self.ai.detector.shape) * 0 + 1
        self.corrections['flat'] = self.ai.flatfield
        self.corrections['absorption'] = make_detector_atten(ene_ev, self.ai.twoThetaArray(self.ai.detector.shape))
        for i in self.corrections.keys():
            if self.corrections[i] is None:
                self.checksums[f'{i}_checksum'] = None
            else:
                self.checksums[f'{i}_checksum'] = pyFAI.utils.calc_checksum(self.corrections[i])
        self._timeit('CdTe LAC and checksums')


    def set_gpu(self, datafile=None):
        self.flags['gpudecompress'] = False
        if self.flags['case'] not in ['integrate','sigma_clip'] or self.args.nogpu is not None:
            LGR.warning(f'No GPU optimization possible for case {self.flags["case"]} and/or --nogpu={self.args.nogpu}')
            return
        else:
            if self.aimethod.target_type == 'gpu' and self.aimethod.impl_lower == 'opencl':# and self.aimethod.algo_lower == 'csr':
                if datafile is None: datafile = self.somefile
                ###--- init engine
                engine = self.ai.engines[self.aimethod].engine
                ###--- make decompressor
                with h5py.File(datafile, 'r') as h:
                    ds, i = h[self.arraypath], 0
                    frame = ds[i]
                    filter_mask, chunk = ds.id.read_direct_chunk(ds.id.get_chunk_info(i).chunk_offset)
                self.gpu.decompressor = BitshuffleLz4(len(chunk), frame.size, dtype=frame.dtype, ctx=engine.ctx)
                LGR.debug('Tune the decompressor for fastest speed:')
                best, wg_use = 999, 0
                for i in range(0, 11):
                    j = 1<<i
                    it = timeit(lambda: self.gpu.decompressor.decompress(chunk, wg=j), number=10)
                    LGR.debug(f'Workgroup size {j:3d} : {it*1e3:.3f}us')
                    if it < best:
                        best = it
                        wg_use = j
                LGR.debug(f'Set decompressor workgroup size (called block in cuda) to {wg_use}')
                self.gpu.decompressor.block_size = wg_use
                ###--- try integrator
                LGR.debug('Tune the integrator for fastest speed:')
                best, wg_use = 999, 0
                frame_d = cla.to_device(engine.queue, frame)
                for i in range(0, 11):
                    j = 1<<i
                    ne = new_engine(engine, j)
                    if self.flags['case']=='integrate':
                        it = timeit(lambda: ne.integrate_ng(frame_d, **self.corrections, **self.checksums), number=10)
                    elif self.flags['case']=='sigma_clip':
                        it = timeit(lambda: ne.sigma_clip(frame_d, **self.corrections, **self.checksums,
                                                          cutoff=self.args.sigmafilter[0], cycle=self.args.sigmafilter[1],
                                                          error_model=self.args.error), number=10)
                    LGR.debug(f'Workgroup size {j:3d} : {it*1e3:.3f}us')
                    if it < best:
                        best = it
                        wg_use = j
                LGR.debug(f'Set the engine workgroup size to {wg_use}')
                ###--- store tuned values
                self.gpu.integ_bloc_size = wg_use
                self.gpu.engine = new_engine(engine, wg_use)
                self.gpu.engine.mask_checksum = pyFAI.utils.calc_checksum(self.ai.mask)
                self.flags['gpudecompress'] = True
                self._timeit(f'Tuning GPU for method {self.aimethod}')
            else:
                LGR.warning(f'No GPU decompression tuning with method {self.aimethod}')
                return


    def set_outfile(self):
        argstring = '\n'
        for arg in self.args.__dict__:
            argstring += f'--{arg} {self.args.__dict__[arg]} \n'
        for sc in self.scans:
            if self.args.force is True and os.path.isfile(self.scans[sc].outname) is True:
                os.remove(self.scans[sc].outname)
            elif self.args.force is False and os.path.isfile(self.scans[sc].outname) is True:
                self.scans[sc].accept = 0
            if self.scans[sc].accept == 1:
                try:
                    os.mkdir(self.args.outdir)
                except FileExistsError as err:
                    LGR.debug(err)
                try:
                    os.mkdir(f'{self.args.outdir}/{self.scans[sc].outsubdir}')
                except FileExistsError as err:
                    LGR.debug(err)
                #os.chmod(self.args.outdir,
                #         stat.S_IRUSR |  stat.S_IWUSR |  stat.S_IRGRP |  stat.S_IWGRP |  stat.S_IROTH)
                os.chmod(self.args.outdir, 0o777)
                os.chmod(f'{self.args.outdir}/{self.scans[sc].outsubdir}', 0o777)
#                         stat.S_IRUSR |  stat.S_IWUSR |  stat.S_IRGRP |  stat.S_IWGRP |  stat.S_IROTH)
                with h5py.File(self.scans[sc].outname, 'w-') as ouf:
                    inf = ouf.create_group('info')
                    inf['cmdline_args'] = argstring
                    inf['gpu_decompression'] = int(self.flags['gpudecompress'])
                    inf['pyfai_method'] = f"""Method(dim=1, split='{self.aimethod.split_lower}', algo='{self.aimethod.algo_lower}', impl='{self.aimethod.impl_lower}', target={self.aimethod.target})"""
                    with h5py.File(self.scans[sc].upperh5, 'r') as fil:
                        inf['start_time'] = fil[f'{self.scans[sc].scan_number}.1/start_time'][()].decode('ascii')
                        inf['end_time'] = fil[f'{self.scans[sc].scan_number}.1/end_time'][()].decode('ascii')
                        try:
                            inf['scan title'] = fil[f'{self.scans[sc].scan_number}.1/title'][()].decode('ascii')
                        except Exception as err:
                            LGR.warning(err)
                    icat = inf.create_group('icat')
                    for kk in self.scans[sc].paths:
                        try:icat[kk] = self.scans[sc].paths[kk]
                        except:LGR.warning(f'Cannot add {kk} to icat')
                    inf.create_dataset('hdintrange version', data=np.array(self.version.encode('ascii'), dtype='S'))
                    inf.create_dataset('datafiles', data=np.array(self.scans[sc].fnames, dtype='S'))
                    inf.create_dataset('total file size (GB)', data=np.array(self.scans[sc].tot_size_gb))
                    inf.create_dataset('calibration_file', data=np.array(os.path.abspath(self.args.ponifile), dtype='S'))
                    inf.create_dataset('mask_file', data=os.path.abspath(self.args.maskfile))
                    inf.create_dataset('wavelength (A)', data=self.ai.wavelength*1e10)
                    inf.create_dataset('S-D distance (mm)', data=self.ai.dist*1e3)
                    inf.create_dataset('pixel size (mm)', data=[self.ai.pixel1*1e3,self.ai.pixel1*1e3])
                    inf.create_dataset('detector attenuation F(2theta)', data=self.det_att)
                    axe = ouf.create_group('positions')
                    mpos = get_positions(sc, self.scans[sc].upperh5,
                                         self.args.mpos + ['fpico2','fpico3','epoch','pico2','pico3',self.args.normcnt],
                                         None if self.args.maxframes==0 else self.args.maxframes)
                    for po in mpos:
                        axe['%s' %(po)] = mpos[po]
                    ouf.create_dataset(self.args.unit, data=np.empty(self.args.npt, dtype='float32'), dtype='float32')
                    ouf[self.args.unit].attrs['radial'] = 1
                    for j in range(self.azims.shape[0]):
                        azg = ouf.create_group(f'azim_{1+j:02d}')
                        azg['azimuthal_range'] = self.azims[j]
                        azg.create_dataset('intensities', data=np.zeros((1, self.args.npt), dtype='float32'),
                                           maxshape=(int(self.scans[sc].tot_n_img*10), self.args.npt),
                                           dtype='float32', chunks=(1, self.args.npt))
                        azg.create_dataset('error', data=np.zeros((1, self.args.npt), dtype='float32'),
                                       maxshape=(int(self.scans[sc].tot_n_img*10), self.args.npt),
                                       dtype='float32', chunks=(1, self.args.npt))
                    if LGR.getEffectiveLevel() in [10]:
                        LGR.debug('Output: %s' %(self.scans[sc].outname))
                        ouf.visit(LGR.debug)
        self._timeit('Initialising output file(s)')


    def switch_azim(self, azim_range, mask=None, mask_checksum=None):

        ### 1) handle possible discontinuity around +/- 180 degrees
        if any(np.absolute(azim_range)>180):
            if self.ai.chiDiscAtPi == False:
                pass
            else:
                if azim_range[0]<0: azim_range[0] = 360+azim_range[0]
                if azim_range[1]<0: azim_range[1] = 360+azim_range[1]
#                self.ai.chiDiscAtPi = False
                with self.ai._sem:
                    self.ai.chiDiscAtPi = False
                    self.ai._cached_array['chi_center'] = None
                    for key in list(self.ai._cached_array.keys()):
                        if isinstance(key, str):
                            if key.startswith('corner'):
                                self.ai._cached_array[key] = None
                LGR.debug(f'Azimuth range changed to {azim_range}')
                LGR.debug(f'ChiDisc at Pi: {self.ai.chiDiscAtPi}')

        else:
            if self.ai.chiDiscAtPi == True:
                pass
            else:
                # self.ai.chiDiscAtPi = True
                with self.ai._sem:
                    self.ai.chiDiscAtPi = True
                    self.ai._cached_array['chi_center'] = None
                    for key in list(self.ai._cached_array.keys()):
                        if isinstance(key, str):
                            if key.startswith('corner'):
                                self.ai._cached_array[key] = None
                LGR.debug(f'ChiDisc at Pi: {self.ai.chiDiscAtPi}')

        ### 2) handle GPU-decompression case and "normal" case separately
        if self.flags['gpudecompress'] is False:
            return azim_range
        else:
            if mask is None:
                mask = self.ai.mask
                mask_checksum = self.gpu.engine.mask_checksum
            else:
                mask_checksum = pyFAI.utils.calc_checksum(mask)
            azim_radians = (np.radians(azim_range[0]), np.radians(azim_range[1]))
            cyintegr = self.ai.setup_sparse_integrator(shape=mask.shape,
                               npt=self.args.npt, unit=self.args.unit,
                               pos0_range=self.si_xrange,
                               pos1_range=azim_radians,
                               mask=mask, mask_checksum=mask_checksum,
                               split=self.aimethod.split_lower,
                               algo=self.aimethod.algorithm,
                               scale=False)
            csr_integr = self.aimethod.class_funct_ng.klass(cyintegr.lut,
                              image_size = mask.size,
                              checksum = cyintegr.lut_checksum,
                              empty=0,
                              unit = self.args.unit,
                              bin_centers = cyintegr.bin_centers,
                              platformid = self.aimethod.target[0],
                              deviceid = self.aimethod.target[1],
                              mask_checksum = cyintegr.mask_checksum)
            return new_engine(csr_integr, self.gpu.integ_bloc_size)


    def process_data(self, scan_obj=None):
        if scan_obj is None or scan_obj.accept==0:
            return

        LGR.debug('___ 1 setting time parameters ___')
        LGR.debug('___ 2 opening output file in append mode ___')
        ouf = h5py.File(scan_obj.outname, 'a')
        if self.args.maxframes > 0:
            max_vlen = self.args.maxframes
            LGR.debug(f'Requested {max_vlen} frames of {scan_obj.tot_n_img} available')
        else:
            max_vlen = scan_obj.tot_n_img
            LGR.debug(f'Requested all available frames ({scan_obj.tot_n_img})')
        pbar = tqdm(total=int(max_vlen*len(self.azims)), unit='f', mininterval=0.5)
        #####----------------------

        LGR.debug(f'___ 3 function:{self.flags["case"]} \\ gpu-decomp:{self.flags["gpudecompress"]} \\ processor:{self.flags["processor"]} ___')

        if self.flags['gpudecompress'] is True and self.flags['case'] in ['integrate','sigma_clip']:
            for w in range(len(self.azims)):
                ytarget = ouf[f'azim_{1+w:02d}/intensities']
                etarget = ouf[f'azim_{1+w:02d}/error']
                ytarget.resize(( max_vlen, self.args.npt))
                etarget.resize(( max_vlen, self.args.npt))
                LGR.debug(f'New output shape {ytarget.shape}')
                self.gpu.engine = self.switch_azim(self.azims[w])
                pbar.set_description_str(f'azim {1+w:02g}/{len(self.azims):02g}')
                if self.flags['case'] == 'integrate':
                    caseargs = {}
                    func = self.gpu.engine.integrate_ng
                elif self.flags['case'] == 'sigma_clip':
                    caseargs = {'cutoff':self.args.sigmafilter[0],
                                'cycle': int(self.args.sigmafilter[1]),
                                'error_model':self.args.error}
                    func = self.gpu.engine.sigma_clip
                next_frame = 0
                for u in range(len(scan_obj.fnames)):
                    with h5py.File(scan_obj.fnames[u], 'r') as fil:
                        ds = fil[self.arraypath]
                        nframes = ds.id.get_num_chunks()
                        for v in range(nframes):
                            filter_mask, chunk = ds.id.read_direct_chunk(ds.id.get_chunk_info(v).chunk_offset)
                            result = func(self.gpu.decompressor(chunk),
                                          **self.corrections,
                                          **self.checksums,
                                          **caseargs)
#                            LGR.debug(f'{int((u*nframes)+v)}, {u}, {v}, {nframes}, {next_frame}')
                            ytarget[int((u*nframes)+v), :] = result.intensity
                            etarget[int((u*nframes)+v), :] = result.sigma
                            pbar.update(1)
                            next_frame += 1
                            if next_frame == max_vlen:break
                        else:
                            continue
                        break
                LGR.debug(f'Stopped at {next_frame} frames, file {u}, frame {v}')
            ouf[self.args.unit][:] = xconv(result.position, self.args.unit)

        elif self.flags['gpudecompress'] is False and self.flags['case'] in ['integrate','sigma_clip']:
            for w in range(len(self.azims)):
                ytarget = ouf[f'azim_{1+w:02d}/intensities']
                etarget = ouf[f'azim_{1+w:02d}/error']
                ytarget.resize(( max_vlen, self.args.npt))
                etarget.resize(( max_vlen, self.args.npt))
                LGR.debug(f'New output shape {ytarget.shape}')
                newazim = self.switch_azim(self.azims[w])
                pbar.set_description_str(f'azim {1+w:02g}/{len(self.azims):02g}')
                if self.flags['case'] == 'integrate':
                    caseargs = {}
                elif self.flags['case'] == 'sigma_clip':
                    caseargs = {'thres'    :self.args.sigmafilter[0],
                                'max_iter' :int(self.args.sigmafilter[1])}
                func = self.flags['function']
                next_frame = 0
                for u in range(len(scan_obj.fnames)):
                    with h5py.File(scan_obj.fnames[u], 'r') as fil:
                        ds = fil[self.arraypath]
                        nframes = ds.shape[0]
                        for v in range(nframes):
                            result = func(ds[v], npt=self.args.npt,
                                          polarization_factor=self.pola,
                                          method=self.aimethod,
                                          radial_range=self.args.xrange,
                                          unit=self.args.unit,
                                          mask=self.ai.mask,
                                          flat=self.ai.flatfield,
                                          correctSolidAngle=self.args.solidangle,
                                          azimuth_range=newazim,
                                          error_model=self.args.error,
                                          **caseargs)
                            ytarget[int((u*nframes)+v), :] = result.intensity
                            etarget[int((u*nframes)+v), :] = result.sigma
                            pbar.update(1)
                            next_frame += 1
                            if next_frame == max_vlen:break
                        else:
                            continue
                        break
                LGR.debug(f'Stopped at {next_frame} frames, file {u}, frame {v}')
            ouf[self.args.unit][:] = result.radial

        elif self.flags['case'] == 'medfilt':
            for w in range(len(self.azims)):
                ytarget = ouf[f'azim_{1+w:02d}/intensities']
                etarget = ouf[f'azim_{1+w:02d}/error']
                ytarget.resize(( max_vlen, self.args.npt))
                etarget.resize(( max_vlen, self.args.npt))
                LGR.debug(f'New output shape {ytarget.shape}')
                pbar.set_description_str(f'azim {1+w:02g}/{len(self.azims):02g}')
                newazim = self.switch_azim(self.azims[w])
                next_frame = 0
                for u in range(len(scan_obj.fnames)):
                    with h5py.File(scan_obj.fnames[u], 'r') as fil:
                        ds = fil[self.arraypath]
                        nframes = ds.shape[0]
                        ytarget.resize(( min([max_vlen,nframes*(u+1)]), self.args.npt))
                        etarget.resize(( min([max_vlen,nframes*(u+1)]), self.args.npt))
                        LGR.debug(f'New output shape {ytarget.shape}')
                        for v in range(min([max_vlen, len(ds)])):
                            result = self.ai.medfilt1d(ds[v], npt_rad=self.args.npt,
                                                       npt_azim=self.args.medianfilter[2],
                                                       percentile=(self.args.medianfilter[0],self.args.medianfilter[1]),
                                                       polarization_factor=self.pola,
                                                       method=self.aimethod,
                                                       radial_range=self.args.xrange,
                                                       unit=self.args.unit,
                                                       mask=self.ai.mask,
                                                       flat=self.ai.flatfield,
                                                       correctSolidAngle=self.args.solidangle,
                                                       azimuth_range=newazim)
                            ytarget[int((u*nframes)+v), :] = result.intensity
                            etarget[int((u*nframes)+v), :] = np.zeros(result.intensity.shape)+1e-2
                            pbar.update(1)
                            next_frame += 1
                            if next_frame == max_vlen:break
                        else:
                            continue
                        break
                LGR.debug(f'Stopped at {next_frame} frames, file {u}, frame {v}')
            ouf[self.args.unit][:] = result.radial

        else:
            LGR.critical('Nothing to do')

        pbar.close()
        LGR.critical(f'Saved {scan_obj.outname}')

        if self.args.avexy is not None:
            if self.args.avexy == 'average':
                func = np.average
            elif self.args.avexy == 'median':
                func = np.median
            max_size_gb = memsize()[1] / (1<<20)
            xyname = scan_obj.outname.replace('.h5', '.xye')
            try:
                LGR.debug(f'Available norm counters: {[kk for kk in ouf["positions"].keys()]}')
                LGR.debug(f'Selected norm counter: {self.args.normcnt}')
                wts = ouf[f'positions/{self.args.normcnt}'][:max_vlen]
                if self.args.norm <= 0:
                    norm = np.median(wts) / wts
                else:
                    norm = self.args.norm / wts
            except KeyError:
                LGR.warning(f'Found no diode normalisation values. Assuming same monitor counts for each pattern')
                wts = np.ones(ouf[f'azim_{(1+w):02d}/intensities'].shape[0])
                norm = wts
            try:
                if self.args.normcnt in ['pico2','pico3']:
                    norm /= ouf['positions/exposure time'][()]
            except KeyError:
                LGR.warning('Found no exposure time. Assuming same time for each pattern')

            norm = np.reshape(norm, (-1,1))
            LGR.debug(f'Normalisation factors now {len(norm.shape)}-dimensional')
            for w in range(len(self.azims)):
                if len(self.azims)>1:
                    azname = xyname.replace('azint',f'azint{1+w:02d}')
                else:
                    azname = xyname
                yout = func(ouf[f'azim_{(1+w):02d}/intensities'][()] * norm, axis=0)
                eout = func(ouf[f'azim_{(1+w):02d}/error'][()] * norm, axis=0)
                LGR.debug('Calculated weighted normalised average')
                np.savetxt(azname, np.transpose([ouf[self.args.unit], yout, eout]), fmt='%.5f')
                LGR.critical(f'Saved {azname:s}')

        else:
            pass
        ouf.close()
        self._timeit('Integration time')
        return


#
#

def main():
    global LGR
    sos = logging.StreamHandler(stream=sys.stdout)
    sos.setFormatter(CustomFormatter())
    LGR = logging.getLogger('LGR')
    LGR.addHandler(sos)
    print('\n')
    wargs = parse_arguments()
    sos.setLevel(wargs.v)
    az = Integrator(wargs)
    az.set_azims()
    az.set_filelist()
    if any([az.scans[sc].accept for sc in az.scans]) is True:
        az.set_ai()
        az.set_method()
        az.set_corrections()
        az.set_gpu()
        az.set_outfile()
        for sc in sorted(az.scans):
            try:
                az.process_data(az.scans[sc])
            except Exception as err:
                LGR.warning(f'Could not finish processing {sc}:')
                LGR.exception(err)
                LGR.exception(traceback.format_exc())
            except KeyboardInterrupt:
                LGR.warning(f'{sc} skipped by user. Moving on or exiting...')
    print('\n')

if __name__ == '__main__':
    main()
