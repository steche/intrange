import os, sys
from re import sub as regexsub
from pathlib import Path
import numpy as np
import argparse
import logging
from hdintrange.browse import whereami
from glob import glob


LG2 = logging.getLogger('LGR')


def parse_arguments():
    parser = argparse.ArgumentParser(description='Integrate azimuthal slices')
    parser.add_argument('datadirs', type=str, nargs='+',
                        help='Names of input directories. They can be absolute or relative paths, \
                        as long as they end with the "scan" folder (scan0001, scan000?, scan*, etc). \
                        If the program is run from a directory other than experiment home directory, \
                        absolute paths are of course necessary. Processing data from multiple proposals \
                        is not supported.')
    parser.add_argument('-p','--ponifile', type=str, nargs='?', default=None,
                        help='Calibration file obtained from pyFAI')
    parser.add_argument('-m','--maskfile', type=str, nargs='?', default=None,
                        help='Boolean image file with same shape as the detector used')
    parser.add_argument('-u','--unit', type=str, nargs='?', default='q_A^-1',
                        choices=['q_A^-1', '2th_deg'],
                        help='Radial units (constant bin size). Default: q_A^-1')
    parser.add_argument('-x', '--xrange', type=str, nargs=2, default=None,
                        help='Minimum and maximum value of x-axis in the chosen radial units. \
                               Default: None, use all available range.')
    parser.add_argument('-n','--npt', type=int, nargs='?', default=2048,
                        help='Number of radial bins. Default: 2048')
    parser.add_argument('-a','--azim', type=str, nargs=3, default=(-180, 180, 1),
                        metavar=('MIN_AZIM','MAX_AZIM','NB_SLICES'),
                        help='Azimuthal range: lower end of the first slice, \
                        higher end of the last slice, number of azimuthal slices. Default: -180 180 1;  \
                        Note: N = -90, W = (-)180, S = 90, E = 0')
    parser.add_argument('--norm', type=float, nargs='?', default=0,
                        help='Multiplicative constant for pattern intensities \
                              after dividing by diode, e.g. 1e7. Applicable to ascii (.xye) output, not in hdf5 array\
                              Default: 0, meaning the median value of fpico3 in the scan is used.')
    parser.add_argument('--normcnt', type=str, nargs='?', default='fpico3',
                        help='Counter used for normalisation. Same conditions as --norm apply here. \
                              Default: fpico3')
    parser.add_argument('--split', type=str, nargs='?', default='no', choices=['no','full'],
                        help='--split full to use full pixel splitting; --split no to avoid it.')
    parser.add_argument('--solidangle', type=str, nargs='?', default='yes', choices=['no','yes'],
                        help='Turn on or off pyFAI solid angle correction. Turn off may help correcting highward slope at high Q in \
                              measurements where detector is very close, e.g. 50 keV and 150 mm sdd')
    parser.add_argument('--medianfilter', type=float, nargs=3, default=None,
                        metavar=('CUT_LOW', 'CUT_HIGH', 'NUMBER_AZIMUTH_BINS'),
                        help='Lower and upper percentiles (0-100) for trimming outliers in each radial bin, \
                        followed by number of azimuthal bins over which to compute trimmed mean. \
                        The underlying function, medfilt1d is incompatible with \
                        GPU decompression, GPU integration, and uncertainty estimation. \
                        Default: None -> use integrate1d with no filtering')
    parser.add_argument('--sigmafilter', type=float, nargs=2, default=None,
                        metavar=('NUMBER_SIGMAS', 'NUMBER_ITERATIONS'),
                        help='Number of standard deviations (sigmas) symmetric around the central value \
                        used to filter outliers in each radial bin, \
                        followed by number of iterations for sigma clipping (typically 5-10). \
                        If both --medianfilter and --sigmafilter are specified, only sigmafilter is retained. \
                        This option automatically turns on error estimation because output intensities depend on it and turns off pixel splitting. \
                        Default: None -> use integrate1d with no filtering')
    parser.add_argument('-o', '--outdir', type=str, nargs='?', default='',
                        help='Directory for processed dirs/files, necessarily located at the main proposal level and starting with processed_. \
                              Examples: -o None --> ihma136/id15/processed, -o 1 --> ihma136/id15/processed_1, -o jq --> ihma136/id15/processed_jq, etc. \
                              Absolute paths, paths to subdirectories, and special characters are stripped away.\
                              Default: <main proposal dir>/processed')
    parser.add_argument('-s', '--session', type=int, nargs='?', default=None,
                        help='Session directory for processed dirs/files, necessarily located at the main proposal level and starting with processed_. \
                              if not specified, the processed folder will be located in the same session as the data being processed. \
                              The trouble is that when experiment folders are restored they are not writeable. So a new, writeable "session" directory has to be made. \
                              The argument therefore must be a date in YYYYMMDD format, e.g. 20220202. \
                              Also, the date has to make sense because it will decide on the archiving date of the directory, \
                              e.g. 20220202 ---> archived on 5 may 2022. Though it is tempting, do not try setting dates far in the past or far in the future. \
                              Something in between the experiment session and the current date, closer to the current date, will do. \
                              Examples: -s None --> ihma136/id15a/20220222/processed, -s 20220619  --> ihma136/id15a/20220619/processed \
                              Extra suffixes will be reintroduced at some point. \
                              Default: <main proposal dir>/processed')
    parser.add_argument('--camera', type=str, nargs='?', default='pilatus', choices=['pilatus','Pilatus','eiger'],
                        help='Detector name if reading hdf5 datafile structure. Default: pilatus. Might be updated in the future to support more detectors.')
    parser.add_argument('-v', type=str, nargs='?', default='warning', choices=['warning','info','debug'],
                        help='Set verbosity level. -v warning: only exceptions; -v info: most setup and integration details; -v debug: lower-level setup details.')
    parser.add_argument('-f', '--flat', type=str, nargs='?', default=None,
                        help='Path to .edf file containing flood correction. Default:None. Quick start: /users/opid15/Floods/Flood_60keV.edf')
    parser.add_argument('--mpos', type=str, nargs='+', default=[],
                        help='Names of motor positions to save in output file, e.g. hrz hrrz hry. Default: None. \
                              Scan start/end time and diodes (per unit time) saved by default.')
    parser.add_argument('--force', type=bool, nargs='?', default=False, const=True,
                        help='Overwrite existing output file. Usage: --force. Default: False.')
    parser.add_argument('--avexy', type=str, nargs='?', default=None, const='average',
                        choices=['average','median'],
                        help='Output an xye column file with the average or median image \
                              of the pilatus_0000.h5 file. Specify "--avexy median", \
                              or use "--avexy" with no argument to use the average. Default: None, no output.')
    parser.add_argument('--error', type=str, nargs='?', default=None, const='poisson', choices=[None,'poisson','azimuthal','hybrid'],
                        help='Save error on intensities based on pyfai models. \
                              Medianfilter switches off error estimation. \
                              Sigma clipping switches on "hybrid" error estimation but "poisson" is also available. \
                              Usage: "--error hybrid" or "--error poisson". Default: None.')
    parser.add_argument('--expect', type=int, nargs='?', default=1,
                        help='Number of images expected for a scan object. \
                        The condition is evaluated at class function level (set_filelist). \
                        If specified (expect>1), only integrate scans with that number of images. \
                        If unspecified (expect=1), integration goes on regardless. \
                        Use zero or a negative value for a dry run')
    parser.add_argument('--nogpu', type=str, nargs='?', default=None, const='yes',
                        help='Specify --nogpu in order to skip GPU decompression, \
                              which is slower but may prevent errors in certain situations. \
                              Use --nogpu to output uncertainties with integrate1d. \
                              This is always applied with --medianfilter')
    parser.add_argument('--cython', type=str, nargs='?', default=None, const='yes',
                        help='Specify --cython in order to avoid opencl implementations, \
                              which is slower but should work in case of problems. \
                              This option implies --nogpu as well.')
    parser.add_argument('--maxframes', type=int, nargs='?', default=0,
                        help='Number of pilatus_*.h5 files to include in a program execution (i.e. across \
                              any scans provided as argument. Default: 0 = include all available files.')
    parser.add_argument('--nexusdetfile', type=str, nargs='?', default=os.path.dirname(__file__)+'/id15_pilatus.h5',
                        help='Detector definition alternative to default (id15_pilatus.h5), e.g. for ROI or a new detector')
    args = parser.parse_args()
    LG2.debug(str(args))

## Logger
    args.v = getattr(logging, args.v.upper())
    LG2.setLevel(args.v)
## Home directory
    try:
        LG2.debug('calling whereami with working directory')
        args.proposal = whereami(os.path.abspath('.'))['homedir']
    except:
        LG2.warning('Will try to get the experiment directory path from input data')
        args.proposal = whereami(args.datadirs[0])['homedir']
## Check poni/mask
    if args.ponifile is not None and args.maskfile is not None:
        try:
            args.ponifile = glob(args.ponifile)[0]
        except IndexError:
            LG2.error(f'{args.ponifile} not found')
            sys.exit(0)
        try:
            args.maskfile = glob(args.maskfile)[0]
        except IndexError:
            LG2.error(f'{args.maskfile} not found')
            sys.exit(0)
    else:
        LG2.error('Missing poni and/or mask file')
        sys.exit(0)
## Check azimuthal
    try:
        args.azim = [float(args.azim[i]) for i in range(3)]
    except ValueError:
        LG2.warning('Invalid azim argument: 3 numbers required. Defaulted to -180 180 1')
        args.azim = [-180, 180, 1]
## Check x-range
    if args.xrange is not None:
        try:
            x0, x1 = float(args.xrange[0]), float(args.xrange[1])
            if 180 >= x1 > x0 >=0:
                args.xrange = (x0, x1)
                LG2.debug(f'xrange: using the range {x0:.3f}-{x1:.3f} {args.unit}')
            else:
                raise ValueError
        except ValueError:
            LG2.warning('Invalid xrange argument. Defaulted to no bounds')
            args.xrange = None
    else:
        LG2.debug(f'xrange: using all available range of {args.unit}')
## Check medianfilter
    if args.medianfilter is not None:
        if (all(args.medianfilter) >= 0) & (all(args.medianfilter[:2]) <= 100):
            args.nogpu = 'yes'
            args.cython = 'yes'
            LG2.debug('Processing automatically set to cython. Medianfilter does not support GPU processing')
            args.medianfilter[0] = int(np.round(args.medianfilter[0],0))
            args.medianfilter[1] = int(np.round(args.medianfilter[1],0))
            args.medianfilter[2] = int(2**(np.round(np.log(args.medianfilter[2])/np.log(2))))
            LG2.debug('Number of azimuthal bins reset to the closest power of 2: {args.medianfilter[2]}')
            if args.medianfilter[1] > 100:
                LG2.warning('Invalid medianfilter argument 1: "cut_high" must be 100 or lower. Reset to 100.')
                args.medianfilter[1] = 100
            elif args.medianfilter[1] <= args.medianfilter[0]:
                LG2.warning(f'Invalid medianfilter arguments: value of "cut_high" must be higher than "cut_low". Skipping medianfilter.')
            else:
                LG2.debug(f'Using medianfilter arguments {args.medianfilter}')
        else:
            for kk, par in enumerate(args.medianfilter):
                if par < 0:
                    LG2.warning(f'Invalid medianfilter argument {kk}: value must be between 0 and 100. Skipping medianfilter.')
                else:
                    pass
## Check sigmafilter
    if args.sigmafilter is not None:
        args.split = 'no'
        LG2.debug(f'Pixel split automatically set to "{args.split}"')
        args.error = 'hybrid'
        LG2.debug(f'Error model automatically set to "{args.error}"')
        if (all(args.sigmafilter) > 0):
            if args.sigmafilter[1] > 256:
                args.sigmafilter[1] = 256
                LG2.warning('Invalid sigmafilter argument 1: "number_iterations" must be 256 or lower. Reset to 256.')
            else:
                if args.medianfilter is not None:
                    LG2.warning('Both medianfiltem and sigmafilter parameters specified. \
                                Using sigmafilter only. To use medianfilter, amend your input arguments and retry.')
                else:
                    LG2.debug(f'Using sigmafilter arguments {args.sigmafilter}.')
        else:
            for kk, par in enumerate(args.sigmafilter):
                if par <= 0:
                    LG2.warning(f'Invalid sigmafilter argument {kk}: value must be positive. Skipping sigmafilter.')
                else:
                    pass
## Check solid angle
    if args.solidangle == 'yes':
        args.solidangle = True
    elif args.solidangle == 'no':
        args.solidangle = False
## Check cython
    if args.cython is not None:
        args.nogpu = 'yes'
    else:
        pass
## Check expect/maxfiles
    if args.maxframes == 0:
        LG2.debug(f'Not limiting input frames (maxframes={args.maxframes:d})')
    else:
        LG2.debug(f'Limit input frames to {args.maxframes:d}')
## Check outdir

    if args.outdir == '':
        args.outdir = 'processed'
    else:
        args.outdir = args.outdir.split('processed_')[-1]  # strip redundant chars
        args.outdir = args.outdir.split('/')[-1]  #strip abspath and subdirs
        args.outdir = regexsub(r'[^A-Za-z0-9]+', '', args.outdir)  # strip all non-alfanumeric characters
        args.outdir = f'processed_{args.outdir}'

    session = whereami(args.proposal, sample=False)['session']
    if args.session is None:
        outsupdir = args.proposal
        LG2.debug(f'Default outsupdir: {outsupdir}')
    else:
        if str(args.session).isdecimal() and str(args.session).startswith('202') and len(str(args.session))==8:
            outsupdir = args.proposal.replace(session, str(args.session))
            LG2.debug(f'New outsupdir: {outsupdir}')
            try: # NEW FOLDER case
                os.mkdir(outsupdir)
            except FileExistsError as err:   # NORMAL case
                LG2.debug(err)
        else:
            LG2.error(f'Invalid outdir argument: {args.session} is not a valid YYYYMMDD date')
            sys.exit(0)

    if isinstance(outsupdir, str):
        readable = os.access(outsupdir, os.R_OK)
        writeable = os.access(outsupdir, os.W_OK)
        owner = Path(outsupdir).owner()
        if owner != 'backup' and writeable is True:  # experiment folder is CURRENT, not RESTORED
            try: # NEW FOLDER case
                os.mkdir(f'{outsupdir}/{args.outdir}')
            except FileExistsError as err:   # NORMAL case
                LG2.debug(err)
            finally:
                args.outdir = f'{outsupdir}/{args.outdir}'
        else:
            LG2.error(f'No write permission on {outsupdir}')
            LG2.error(f'Change session from {session} to something else using the -s argument and retry')
            sys.exit(0)


## Print out all input (regardless of verbosity setting)
    LG2.critical('== Input arguments =============================')
    for i in [('proposal', args.proposal),
              ('data directories', args.datadirs),
              ('expected frames per scan', args.expect),
              ('stop process at N frames', args.maxframes),
              ('poni file', args.ponifile),
              ('mask file', args.maskfile),
              ('flat-field file', args.flat),
              ('azimuth ranges', args.azim),
              ('radial units', args.unit),
              ('radial bins', args.npt),
              ('radial range', args.xrange),
              ('error model', args.error),
              ('pixel split', args.split),
              ('solid angle', args.solidangle),
              ('filter sigma', args.sigmafilter),
              ('filter percentile', args.medianfilter),
              ('skip GPU decompression', args.nogpu),
              ('use cython not opencl', args.cython),
              ('normalisation factor', f'{args.norm:.3e}'),
              ('normalisation counter', f'{args.normcnt}'),
              ('motor positions', args.mpos),
              ('output session', args.session),
              ('output directory', os.path.abspath(args.outdir)),
              ('force overwrite', args.force),
              ('verbosity', args.v),
              ('camera name', args.camera),
              ('nexus detector', os.path.basename(args.nexusdetfile))]:
        LG2.critical( f'{i[0]:24s}: {i[1]}')
    LG2.critical('===============================================')
    return args
