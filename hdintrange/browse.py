import sys, os
import logging
import numpy as np
from glob import glob
from time import strptime


LG2 = logging.getLogger('LGR')


def path_until(pathsplit, level):
    """
    Given a list of strings obtained from breaking down a path,
    the function returns the path joined only up to a certain level
    Level 0 is /data
    Level 1 is visitor or inhouse
    Level 2 is proposal number or id15
    Level 3 is id15 or inhouse proposal number
    Level 4 is session (new visitor) or sample (old visitor) or id15 (old inhouse)
    Level 5 is sample or dataset or sample
    ...and so on
    """
    if level < 0:
        LG2.error('Level must be 1 or greater')
        return
    else:
        return '/'+'/'.join(pathsplit[:level+1])


def split_path(work_dir):
    """
    splits the working directory by normal slash (/)
    and returns a list of the elements
    """
    return [i for i in os.path.abspath(work_dir).split('/') if i != '']


def whereami(wd, sample=False):
    """
    Returns proposal type, name, and the level of an absolute directory path in the Bliss data policy
    If paths start with /gpfs/easy/data/id15...; it's simpler to remove the disk name (/gpfs/easy part)
    """
    # 0) initial check of the input path. "wd" may or may not be a path in the ESRF system
    pathsplit = split_path(wd)
    if pathsplit[0] == 'gpfs' and pathsplit[3] in ['visitor','id15','id15a']:
        pathsplit = pathsplit[2:]
    LG2.debug(f'whereami: {wd}')
    LG2.debug(f'Matching paths with {str(pathsplit)}')

    # 1) this catches nonexistent paths
    for j in range(len(pathsplit)):
        temppath = '/'+'/'.join(pathsplit[:j+1])
        try:
            match = glob(temppath)[0]
            LG2.debug(f'Found {match}')
        except IndexError:
            LG2.error(f'{temppath} not found')
            lastvalid = '/'+'/'.join(pathsplit[:j])
            LG2.critical(f'Longest valid path: {lastvalid}')
            LG2.critical('Maybe you mean one of these:')
            for kk in glob(f'{lastvalid}/*'):
                LG2.critical(f' * {kk}')
            # print('\n')
            sys.exit(0)
    pathsplit = split_path(match)
    LG2.debug(f'Updated pathsplit: {str(pathsplit)}')
    paths = {}
    paths['len'] = len(pathsplit)
    paths['workdir'] = wd

    # 2) this catches paths with right syntax but invalid content
    try:
        if 'visitor' in match:
            vpos = pathsplit.index('visitor')
            paths['type'] = 'visitor'
            paths['proposal'] = pathsplit[vpos+1]
            paths['beamline'] = pathsplit[vpos+2]
            try:
                session = pathsplit[vpos+3]
                LG2.debug(f'Session string: {session}')
            except IndexError:   # try looking for the session name in case it was not provided
                subfolds = os.listdir(path_until(pathsplit,3))
                LG2.debug(f"Looking for the session name to match visitor path format: ls . = {subfolds}")
                session = ''
                for dd in subfolds:
                    if dd[:3] in ['202','203','204']:
                        session = dd
                        break
                    else:
                        session = ''
                # if pathsplit[vpos+3] == time.strftime('%Y%m%d', time.localtime(os.path.getctime(path_until(pathsplit, vpos+3)))):  # compare creation time with session date
#            if session.isdecimal() and session.startswith('202') and len(session)==8:  # dumb but effective comparison
            if session.isdecimal() and session.startswith('20') and len(session)==8 and strptime(session, '%Y%m%d'):
                LG2.debug(" ____ General case: visitor and inhouse data as of may 2022 are saved in /data/visitor/<proposal>/<beamline>/<session>, where <session> is the YYYYMMDD date of the first day of beamtime")
                paths['session'] = session
                paths['homedir'] = glob(f"/data/visitor/{paths['proposal']}/{paths['beamline']}/{paths['session']}/")[0]  #redundant but will catch errors (i.e. other beamline numbers)
                LG2.debug(f'FOUND {str(paths)} (current visitor format)')
            else:
                LG2.debug(" ____ Old visitor case: /data/visitor/<proposal>/<beamline>/ format used until may 2022")
                LG2.debug(f'Not a valid session name: {session}. Is this an old experiment?')
                paths['session'] = ''
                paths['homedir'] = glob(f"/data/visitor/{paths['proposal']}/{paths['beamline']}/")[0]
                LG2.debug(f'FOUND {str(paths)} (legacy visitor format)')
        elif 'inhouse' in match:
            LG2.debug(" ____ Inhouse case: /data/id15/<inhouse_disk>/<proposal>/id15/ format, still needed in case previous inhouse experiments need processing. No new inhouse experiments as of may 2022.")
            for i, pth in enumerate(pathsplit):
                if bool('inhouse' in pth) is True:
                    vpos = i
                    paths['type'] = pth
                    LG2.debug(f'paths: {paths}, vpos: {vpos}, pathsplit: {pathsplit}')
            paths['proposal'] = pathsplit[vpos+1]
            paths['beamline'] = pathsplit[vpos+2]
            paths['session'] = None # None should be handled by browse_arguments 2025-feb-24
            mergedpaths = f"/data/id15/{paths['type']}/{paths['proposal']}/{paths['beamline']}"
            print(mergedpaths)
            LG2.debug(f"{paths}")
            paths['homedir'] = glob(mergedpaths)[0]
#            paths['session'] = ''
#            paths['homedir'] = glob(f"/data/{paths['beamline']}/{paths['type']}/{paths['proposal']}/{paths['beamline']}/")[0]
            LG2.debug(f'FOUND {str(paths)} (inhouse format)')
        else:
            LG2.debug(" ____ Local case: when analysing a working directory outside an experiment this should give a warning but not break the program because the path information can be recovered from the absolute path of input data, if provided.")
            raise ValueError
        for kk in paths:
            LG2.debug(f' >> {kk:12s} : {paths[kk]}')
    except (NameError, IndexError) as err:
        msg = f'{err}. Your working directory does not match a full experiment data path. {str(pathsplit)}'
        LG2.error(msg)
    except ValueError as err:
        msg = f'{err}. Your working directory does not match any ID15 experiment. {str(pathsplit)}'
        LG2.warning(msg)
        raise ValueError.with_traceback()

    # 3) by now the path exist but it might be incomplete, e.g. providing only sample/dataset
    if sample == True:
        if 'RAW_DATA' in wd:   # new icat format from october 2024
            pathsplit = [i for i in wd.split(paths['homedir'])[-1].split('/') if i != '']
            LG2.debug(str(pathsplit))
            paths['rawcontainer'] = pathsplit[0]
            paths['sample'] = pathsplit[1]
            paths['dataset'] = pathsplit[2]
            paths['datasetshort'] = paths['dataset'].replace(f"{paths['sample']}_",'', 1)
            paths['upperh5'] = '/'.join([paths['homedir'],paths['rawcontainer'],paths['sample'],paths['dataset'],paths['dataset']])+'.h5'
            try:
                paths['scan'] = pathsplit[3]
            except IndexError:
                LG2.error('Invalid input: no scan provided. Input must be a path in the format sample/dataset/scan')
        else: # format until september 2024
            pathsplit = [i for i in wd.split(paths['homedir'])[-1].split('/') if i != '']
            LG2.debug(str(pathsplit))
            paths['rawcontainer'] = ''
            paths['sample'] = pathsplit[0]
            paths['dataset'] = pathsplit[1]
            paths['datasetshort'] = paths['dataset'].replace(f"{paths['sample']}_",'', 1)
            paths['upperh5'] = '/'.join([paths['homedir'],paths['sample'],paths['dataset'],paths['dataset']])+'.h5'
            try:
                paths['scan'] = pathsplit[2]
            except IndexError:
                LG2.error('Invalid input: no scan provided. Input must be a path in the format sample/dataset/scan')
    return paths
#%%

class Scan:
    """
    *. paths is the output of the function whereami
    *. scankey is the string composed by the Bliss labels sample_dataset_scan, with dataset stripped of the sample repetition
    *. fnames is the list of detector_????.h5 files containing data
    *. outname is the name of the output result file
    *. tot_n_img is the total number of detector images across the list of input .h5 files
    *. accept is the flag signalling whether this scan contains the number of images expected in the cmdline argument --expect
    """
    def __init__(self, scankey, paths, detector):
        self.scankey = scankey
        self.paths = paths
        try:
            self.upperh5 = glob(paths['upperh5'])[0]
        except IndexError:
            LG2.error(f'No such file: {paths["upperh5"]}')
            sys.exit(0)
        self.scan_number = str(int(scankey.split('scan')[-1]))   #e.g. '6'
        ### add sample subdir as an option, to conform to integrator
        self.outsubdir = paths['sample'] + '_' + paths['dataset'].replace(f"{paths['sample']}_", '')
        self.outprefix = f'azint_{scankey}_{detector}.h5'
        self.fnames = []
        self.tot_n_img = 0
        self.accept = 0
        self.tot_size_gb = 0

    def _count(self):
        for ii in self.__dict__:
            LG2.debug(f'{ii} : {getattr(self, ii)}')


def make_filelist_HDF5(datadirs, detector='pilatus'):
    """
    Looks for image files in the dirs input as argument.
    IMPORTANT: argparse already finds scan dirs if they're input as dataset0*/scan*
    paths is the output of the function whereami. A dictionary that looks like {'len': 8, 'workdir': '/data/id15/inhouse5/ihma136/id15/Cr2O3_x1800y32z0_100kev/Cr2O3_x1800y32z0_100kev_0001/scan0001', 'type': 'inhouse', 'level': 'scan', 'homedir': 'data/id15/inhouse5/ihma136/id15', 'proposal': 'ihma136', 'sample': 'Cr2O3_x1800y32z0_100kev', 'dataset': 'Cr2O3_x1800y32z0_100kev_0001', 'datasetshort': '0001', 'scan': 'scan0001'}
    Files are h5 following the nexus format: detector_0002.h5 etc.
    Dirs are named scan0001, scan0002, etc, when argparse is used as specified above.
    Higher-level paths should not be input, as they make the recursive search for files much longer
    """
    scans = {}

    for scandir in sorted(datadirs):
        LG2.debug(f'SCANDIR: {scandir}')
        abspath = os.path.abspath(scandir)
        LG2.debug(f'ABSPATH: {abspath}')
        paths = whereami(abspath, sample=True)
        scankey = f"{paths['sample']}_{paths['datasetshort']}_{paths['scan']}"
        scans[scankey] = Scan(scankey, paths, detector)
        for fil in sorted(glob(f'{abspath}/{detector}_????.h5')):
            scans[scankey].fnames.append(os.path.abspath(fil))
        else:
            for ii in scans[scankey].__dict__:
                LG2.debug(f'{ii} : {getattr(scans[scankey], ii)}')
            continue

    return scans
