import sys, os
import time
from glob import glob
import logging
import numpy as np
import h5py, pyFAI, fabio
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator
from pyFAI.multi_geometry import MultiGeometry
from psutil import virtual_memory
from functools import partial

#global LG2
LG2 = logging.getLogger('LGR')
# reduce output by imported modules
logging.getLogger('pyFAI').setLevel(30)
logging.getLogger('h5py').setLevel(30)
logging.getLogger('fabio').setLevel(30)




##### for testing purposes
# visold = '/data/visitor/ch6210/id15/exp39/exp39_dry/scan0001'
# visnew = '/data/visitor/ihmi1489/id15a/20220531/CeO2/CeO2_0001/scan0001'
# inho = '/data/id15/inhouse6/ihch1573/id15/df1/df1_pdf/scan0001'
# nodata = '/users/opid15/'
# nopath = '/users/opid15/fxe1'

class GPU_prm:
    def __init__(self):
        self.decompressor = None
        self.integ_bloc_size = None
        self.engine = None


def newl():
    print('\n')


def outstream(message):
    sys.stdout.flush()
    sys.stdout.write('%s \r' %message)


def memsize(image_shape=(1679,1475), dtype='uint32'):
    # mem_bytes = os.sysconf('SC_PAGE_SIZE') * os.sysconf('SC_PHYS_PAGES')
    mem_bytes = virtual_memory().available
    im_pix = np.prod(image_shape)
    im_bytes = np.dtype(dtype).itemsize
    mem_frames = mem_bytes / ( im_pix * im_bytes )
    return mem_frames, mem_bytes, im_pix, im_bytes


def show_memsize():
    mem_frames, mem_bytes, im_pix, im_bytes = memsize()
    print(f'Max N frames in memory: {np.round(mem_frames)}')


def frames_in_memory(image_shape=(1679,1475), dtype='uint32'):
    mem_frames, mem_bytes, im_pix, im_bytes = memsize(image_shape, dtype)
    print(f'pixels/frame: {im_pix/1e6:.2f} Mpx')
    print(f'dtype: {dtype} ({im_bytes:d}B/px)')
    print(f'frame size: {np.round(im_pix*im_bytes / (1<<20),0) :.0f} MB')
    print(f'memory size: {np.round( mem_bytes / (1<<20), 0):.0f} MB')
    print(f'number of frames in memory: {np.round(mem_frames, 0):.0f}')


def xconv(x, unit):
    if unit == '2th_deg':
        return np.degrees(x)
    elif unit == 'q_A^-1':
        return 0.1*x


def new_engine(engine, wg):
    return engine.__class__((engine._data, engine._indices, engine._indptr),
                              engine.size, empty=engine.empty, unit=engine.unit,
                              bin_centers=engine.bin_centers, azim_centers = engine.azim_centers,
                              ctx=engine.ctx, block_size=wg)



def get_positions(scankey, h5abspath, axes, maxlength):
    """
    scankey: e.g. "{paths['sample']}_{paths['datasetshort']}_{paths['scan']}"
    h5abspath: e.g. /data/visitor/ch6066/id15/sample1/sample1_dataset4/sample1_dataset4.h5
    axes: e.g. [hry, hrz, temperature, potential, tfy]
    maxlength: if argument maxframes is specified, arrays are clipped to length=maxframes, otherwise maxlength=None (i.e. not clipping)
    """
    out = {}
    LG2.debug(f'Searching {h5abspath} for motor positions')
    scan_number = str(int(scankey.split('scan')[-1]))   #e.g. '6'
    categos = ['measurement','instrument/positioners','instrument/fscan_parameters']
    with h5py.File(h5abspath, 'r') as hf:
        scan_entries = [k for k in hf if k.split('.')[0]==scan_number]   #e.g. ['6.1','6.2']
        try:
            out['exposure time'] = hf[f'{scan_entries[0]}/instrument/pilatus/acq_parameters/acq_expo_time'][()]
        except KeyError:
            out['exposure time'] = 1e-8
        for tgt in axes:
            has_values = 0
            for cat in categos:
                for sn in scan_entries:
                    LG2.debug(f'Looking for {tgt} in {sn}/{cat}')
                    try:
                        if sn[-1] == '2' and tgt not in ['epoch','fpico2','fpico3']:
                            array = hf[f'{sn}/{cat}/{tgt}'][()]
                            timebase2 = hf[f'{sn}/measurement/epoch'][()]
                            timebase1 = hf[f'{sn.replace(".2",".1")}/measurement/epoch_trig'][()]
                            tbase2len = min([len(timebase2), len(array)])
                            arrayinterp = np.interp(timebase1, xp=timebase2[:tbase2len], fp=array[:tbase2len])
                            # out[tgt] = np.round(arrayinterp, 6).astype(np.float64)
                            out[tgt] = np.round(arrayinterp[:maxlength], 6).astype(np.float64)
                        else:
                            # out[tgt] = np.round(hf[f'{sn}/{cat}/{tgt}'][()], 6).astype(np.float64)
                            out[tgt] = np.round(hf[f'{sn}/{cat}/{tgt}'][:maxlength], 6).astype(np.float64)
                        LG2.debug(f'Found {sn}/{cat}/{tgt}: shape {out[tgt].shape}')
                        has_values = 1
                        break   # break sn
                    except KeyError as err:
                        LG2.debug(f'{err}')
                        pass
                    except Exception as err:
                        LG2.debug(f'{err}')
                else:   # this is necessary (tested)
                    continue   # this is necessary (tested)
                break   # break cat   # this is necessary (tested)
            else:  # this is not necessary
                continue   # this is not necessary
            if has_values == 0:
                LG2.warning(f'Not found:{m}')
    return out
#
#
def get_mg_data(scandir, h5abspath):
    """
    Similar to get_positions but safer: gets pilatus data only when arrays are small (e.g. PDF)
    scandir: e.g. "scan0013"
    h5abspath: e.g. /data/visitor/ch6066/id15/sample1/sample1_dataset4/sample1_dataset4.h5
    axes: e.g. [hry, hrz, temperature, potential, tfy]
    """
    out = {}
    n = f"{int(scandir.split('scan')[-1])}.1"
    with h5py.File(h5abspath, 'r') as hf:
        out['exposure time'] = hf[f'{n}/instrument/pilatus/acq_parameters/acq_expo_time'][()]
        out['start_time'] = hf[f'{n}/start_time'][()]
        out['end_time'] = hf[f'{n}/end_time'][()]
        rawdata2d = hf[f'{n}/measurement/pilatus'][()]
        rawfpico3 = hf[f'{n}/measurement/fpico3'][()]
        rawfpico2 = hf[f'{n}/measurement/fpico2'][()]
        npoints = hf[f'{n}/instrument/fscan_parameters/npoints'][()]
        fpp = npoints
        nb_frames = hf[f'{n}/instrument/pilatus/acq_parameters/acq_nb_frames'][()]
        npositions = int(nb_frames/npoints)
        out['pilatus'] = np.empty((npositions, 1679,1475))
        out['fpico3'] = np.empty(npositions)
        out['fpico2'] = np.empty(npositions)
        for nn in range(npositions):
            out['fpico3'][nn] = np.average(rawfpico3[nn*fpp:nn*fpp+fpp])
            out['fpico2'][nn] = np.average(rawfpico2[nn*fpp:nn*fpp+fpp])
            out['pilatus'][nn,:,:] = np.average(rawdata2d[nn*fpp:nn*fpp+fpp, :,:], axis=0)#, weights=rawfpico3[nn*fpp:nn*fpp+fpp])
        for nn in range(npositions):
            out['pilatus'][nn,:,:] = out['pilatus'][nn,:,:] * out['fpico3'][nn] / sum(out['fpico3'])
    return out


class CustomFormatter(logging.Formatter):
    "Customises logging output"
    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    purple = "\x1b[1;35m"
    blue = "\x1b[1;34m"
    light_blue = "\x1b[1;36m"
    reset = "\x1b[0m"
    # fmt = "::%(levelname)s::(%(filename)s:%(lineno)d):: %(message)s "
    fmt = "::%(asctime)s::(%(filename)s:%(lineno)d):: %(message)s "
    short = '-- %(message)s '

    FORMATS = {
        logging.DEBUG: light_blue + fmt + reset,
        logging.INFO: reset + fmt + reset,
        logging.WARNING: yellow + fmt + reset,
        logging.ERROR: red + fmt + reset,
        logging.CRITICAL: reset + short + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt, datefmt='%Y-%m-%d %H:%M:%S')
        return formatter.format(record)
