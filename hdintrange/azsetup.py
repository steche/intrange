import os
import pyFAI
import fabio
import logging
import numpy as np
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator

#global LG2
LG2 = logging.getLogger('LGR')
# reduce output by imported modules
logging.getLogger('pyFAI').setLevel(30)
logging.getLogger('h5py').setLevel(30)
logging.getLogger('fabio').setLevel(30)
#
#
def make_ai(ponifile, maskfile, nexusdetfile=None, flatfile=None):

    ai = AzimuthalIntegrator()
    LG2.debug('1_ai: '+str(ai))

    try:
        det = pyFAI.detector_factory(nexusdetfile)
    except (RuntimeError, TypeError, OSError) as err:
        LG2.debug(f'{err}. Defaulted to id15_pilatus.h5')
        det = pyFAI.detector_factory(os.path.dirname(__file__)+'/id15_pilatus.h5')
    LG2.debug('2_det: '+str(det))

    # msk = np.ascontiguousarray(fabio.open(maskfile).data, np.int8)
    msk = fabio.open(maskfile).data
    det.mask = msk
    LG2.debug('3_det_mask: '+str(det.mask.shape))

    try:
        det.flatfield = fabio.open(flatfile).data
    except FileNotFoundError as err:
        if flatfile is not None:
            LG2.warning(f'{err}. Defaulted to None')
            det.flatfield = np.ones(det.mask.shape)
    except TypeError as err: # arg not set
        LG2.debug('No flood file provided')
        det.flatfield = np.ones(det.mask.shape)
    LG2.debug('4_det_flatfield: '+str(det.flatfield.shape))

    ai.detector = det
    with open(ponifile, 'r') as f:
        lines = f.readlines()
    for l in lines[-7:]:
        LG2.debug(l)
        l = l.replace(':','').strip().split()
        for j in [('Distance','dist'),('Poni1','poni1'),('Poni2','poni2'),
                  ('Rot1','rot1'),('Rot2','rot2'),('Rot3','rot3'),
                  ('Wavelength','wavelength')]:
            if l[0] == j[0]:
                setattr(ai, j[1], float(l[-1]))
                LG2.debug(f'{j[1]}:{float(l[-1])}')
            else:
                LG2.debug(f'{j[1]}:##')
    else:
        LG2.debug(f'Poni file read correctly {ponifile}')
    for j in [('Distance','dist'),('Poni1','poni1'),('Poni2','poni2'),
              ('Rot1','rot1'),('Rot2','rot2'),('Rot3','rot3'),
              ('Wavelength','wavelength')]:
        if getattr(ai, j[1]) is None:
            LG2.error('{j[1]} not set')
    LG2.debug(f'5_final_ai: {ai}')

    return ai
#
#
def make_detector_atten(ene_ev, tth_rad, d_cm=0.1):
    """
    econst (keV/A)
    X = 2theta_rad, 1D or 2D
    mu = linear attenuation coef (1/cm)
    d = sensor thickness (cm)
    K = exp((-mu*d)/cos(X))
    intens_final = intens_ini / K
    """
    cdtefile = os.path.dirname(__file__)+'/cdte_mu_cm.dat'

    try:
        spl_ene, spl_att = np.loadtxt(cdtefile, unpack=True, usecols=(0,1), comments='#')
        mu = spl_att[np.argmin(abs(spl_ene-ene_ev))]
        LG2.debug(f'Found {cdtefile}')

    except OSError as err:
        xcom_ene = 1e3 * np.array([5,6,8,10,15,20,26.71,26.71,30,31.81,31.81,40,
                                   50,60,70,80,90,100,110,120,130,140])
        # mass attenuation coefficients (cm2/g) for CdTe from XCOM database
        xcom_att = np.array([8.392e+02, 5.286e+02, 2.492e+02, 1.381e+02, 4.656e+01, 2.144e+01,
                             9.832e+00, 2.943e+01, 2.182e+01, 1.874e+01, 3.494e+01, 1.930e+01,
                             1.067e+01, 6.542e+00, 4.321e+00, 3.019e+00, 2.206e+00, 1.671e+00,
                             1.305e+00, 1.046e+00, 8.566e-01, 7.152e-01])
        sensor_density = 5.85
        spl = np.interp(ene_ev, xcom_ene, np.log10(xcom_att))
        mu = 10**spl * sensor_density
        LG2.warning(f'{err} Recalculating value for {np.round(ene_ev):.0f} eV')

    LG2.debug(f'CdTe LAC at {np.round(ene_ev):.0f} eV = {mu:.1f} / cm')
    K = (1-np.exp(-mu*d_cm/np.cos(tth_rad))) / (1-np.exp(-mu*d_cm))

    return K
#
#
def make_azims(azim_min, azim_max, nb_slices):
    """
    input arguments: azim_min (float, degrees), azim_max(float, degrees), nb_slices (integer)
    output: list of tuples, each tuple (azim_ini, azim_fin) containing initial,final point of azimuth slices
    """
    pars = {}
    pars['cake'] = ( float(azim_min), float(azim_max) )
    pars['npieces'] = int(nb_slices)
    #-- calculate azim slices
    if pars['npieces'] == 1:
        pars['azims'] = [( pars['cake'][0], pars['cake'][1] )]
    #-- calculate azim slices if n.pieces > 1
    elif pars['npieces'] > 1:
        vals = np.linspace( pars['cake'][0],
                            pars['cake'][1],
                            pars['npieces']+1 )
        pars['azims'] = [ (vals[i],vals[i+1]) for i in range(pars['npieces']) ]

    LG2.debug(f"Using {len(pars['azims'])} azimuthal slices:")
    for i in pars['azims']:
        LG2.debug(f'{i[0]:.3f} - {i[1]:.3f}')
    plusminus = u"\u00B1"
    LG2.debug('_________________[-90]__________')
    LG2.debug('|(-135)            |           |')
    LG2.debug('|                  |           |')
    LG2.debug('|                  |           |')
    LG2.debug('|                  |           |')
    LG2.debug('|                  |      (-45)|')
    LG2.debug('|                  |           |')
    LG2.debug('|                  |           |')
    LG2.debug('|                  |           |')
    LG2.debug('|                  |           |')
    LG2.debug(f'[{plusminus}180]-------------o---------[0]')
    LG2.debug('|                  |           |')
    LG2.debug('|                  |           |')
    LG2.debug('|                  |           |')
    LG2.debug('|                  |           |')
    LG2.debug('|                  |           |')
    LG2.debug('|_________________[90]_________|')
    return np.array(pars['azims'])
