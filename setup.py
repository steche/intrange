from setuptools import setup, find_packages

with open("README.md", "r", encoding="utf-8") as readme:
    long_description = readme.read()
setup(
    name='hdintrange',
    version='0.13.2',
    description='Azimuthal integration for ID15 users',
    license='MIT',
    maintainer='steche',
    long_description=long_description,
    long_description_content_type='text/markdown',
    maintainer_email='stefano.checchia--NoSpam--@esrf.fr',
    install_requires=['pyopencl','psutil','tqdm'],
#    package_data={'':['id15_pilatus.h5','cdte_mu_cm.dat']},
    include_package_data=True,     # If you have extra (non .py) data this should be set to True
    entry_points={'console_scripts': ['hdintrange_v13=hdintrange.hdv13:main','hdintrange=hdintrange.hdv13:main']},   ##command=folder.file:function##
    packages=find_packages(),     # Where to look for the python package
)
